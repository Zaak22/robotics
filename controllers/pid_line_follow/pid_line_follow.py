from controller import Robot

robot = Robot()
timestep = 1

lm = robot.getDevice("left wheel motor")
rm = robot.getDevice("right wheel motor")
lm.setPosition(float('inf'))
lm.setVelocity(0.0)
rm.setPosition(float('inf'))
rm.setVelocity(0.0)

sensors = []
names = ["ir0", "ir1", "ir2", "ir3"]
reading = [0, 0, 0, 0]
pre_reading = [0, 0, 0, 0]
coefficient = [-1000, -100, 100, 1000]
previous_error = 0.0
kp = 4.2
kd = 0.8
ki = 0
Integral = 0.0
speed = 4
side = 0
taken = False
ready = True

for name in names:
    sensor = robot.getDevice(name)
    sensor.enable(timestep)
    sensors.append(sensor)


def get_reading():
    for i in range(len(names)):
        reading[i] = not int(sensors[i].getValue()) > 500


def is_red(value):
    return 360 < value < 380


def move(l_speed, r_speed, t=31):
    lm.setVelocity(l_speed)
    rm.setVelocity(r_speed)
    for i in range(t):
        robot.step(timestep)


def turn_right():
    move(4, 0, 30)


def turn_left():
    move(0, 4)


def stop():
    move(0, 0)


def turn_360():
    move(1, -1, 142)


def go_back():
    move(-4, -4, 28)


def pid():
    global turn, side, taken, ready

    error = 0
    for i in range(0, len(names)):
        error += coefficient[i] * reading[i]

    p = kp * error
    i = Integral + (ki * error)
    d = kd * (error - previous_error)
    correction = (p + i + d) / 1000

    l_speed = speed - correction
    r_speed = speed + correction

    if l_speed < 0.0:
        l_speed = 0
    if l_speed > 6.28:
        l_speed = 6.28
    if r_speed < 0.0:
        r_speed = 0
    if r_speed > 6.28:
        r_speed = 6.28

    if pre_reading == [0, 0, 0, 1] and reading == [0, 0, 0, 1]:
        turn_left()
        side = 1
    elif pre_reading == [1, 0, 0, 0] and reading == [1, 0, 0, 0]:
        turn_right()
        side = 2
    elif reading == [0, 0, 0, 0]:
        if not taken:
            if ready:
                turn_360()
                taken = True
            elif side == 1:  # left
                side = 0
                turn_left()
            elif side == 2:  # right
                side = 0
                turn_right()
        else:
            if ready:
                move(2, 2, 10)
                go_back()
                turn_360()
                taken = False
            elif side == 1:
                side = 0
                turn_left()
            elif side == 2:
                side = 0
                turn_right()

        ready = not ready

    lm.setVelocity(l_speed)
    rm.setVelocity(r_speed)

    return i, error


def check_the_end():
    is_end = True
    for s in sensors:
        val = s.getValue()
        is_end = is_end and is_red(val)

    if is_end:
        move(speed, speed, 25)
        stop()
        return True

    return False


while robot.step(timestep) != -1:

    get_reading()

    Integral, previous_error = pid()

    pre_reading = reading.copy()

    if check_the_end():
        exit()
